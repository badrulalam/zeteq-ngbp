angular.module( 'ngBoilerplate.portfolio', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'portfolio', {
    url: '/portfolio',
    views: {
      "main": {
        controller: 'PortfolioCtrl',
        templateUrl: 'portfolio/portfolio.tpl.html'
      }
    },
    data:{ pageTitle: 'Portfolio' }
  });
})

.controller( 'PortfolioCtrl', function PortfolioCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})

;
