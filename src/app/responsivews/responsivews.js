angular.module( 'ngBoilerplate.responsivews', [
  'ui.router',
  'placeholders',
  'ui.bootstrap',
    'ngAnimate'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'responsivews', {
    url: '/responsivewebsite',
    views: {
      "main": {
        controller: 'ResponsivewsCtrl',
        templateUrl: 'responsivews/responsivews.tpl.html'
      }
    },
    data:{ pageTitle: 'Responsive Website' }
  });
})

.controller( 'ResponsivewsCtrl', function ResponsivewsCtrl( $scope, $timeout ) {
  // This is simple a demo for UI Boostrap.

      $scope.showPopupSms = false;
        $scope.showPopupArt = false;


      $scope.hoverInRW = function(){
        console.log('hovering');
        $scope.showPopupSms = true;

          $timeout(function() {
              $scope.showPopupSms = false;
              console.log('hovering timeout');

          }, 5000);
      };



        $scope.hoverInArt = function(){
            console.log('hovering Art');
            $scope.showPopupArt = true;

            $timeout(function() {
                $scope.showPopupArt = false;
                console.log('hovering timeoutArt');

            }, 5000);
        };


})



    .controller('CarouselDemoCtrl', function ($scope) {
      $scope.myInterval = 5000;
      $scope.noWrapSlides = false;
      var slides = $scope.slides = [];


    })



;
