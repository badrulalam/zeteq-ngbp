angular.module( 'ngBoilerplate.onlinestore', [
  'ui.router',
  'placeholders',
  'ui.bootstrap',
  'ngAnimate'
])

.config(function config( $stateProvider ) {
  $stateProvider
      .state( 'onlinestore', {
    url: '/onlinestore',
    views: {
      "main": {
        controller: 'OnlinestoreCtrl',
        templateUrl: 'onlinestore/onlinestore.tpl.html'
      }
    },
    data:{ pageTitle: 'Onlinestore' }
  })

      .state( 'virtualmp', {
        url: '/virtualmp',
        views: {
          "main": {
            controller: 'VirtualmpCtrl',
            templateUrl: 'onlinestore/vertualmp.tpl.html'
          }
        },
        data:{ pageTitle: 'Vertual marketplace' }
      })



  ;




})



.controller( 'OnlinestoreCtrl', function OnlinestoreCtrl( $scope, $timeout ) {
  // This is simple a demo for UI Boostrap.
      $scope.showPopupBackend = false;

      $scope.hoverInBackend = function(){
        //console.log('hovering');
        $scope.showPopupBackend = true;

        $timeout(function() {
          $scope.showPopupBackend = false;
          //console.log('hovering timeout');

        }, 5000);
      };





})


    .controller( 'VirtualmpCtrl', function VirtualmpCtrl( $scope, $timeout ) {
      // This is simple a demo for UI Boostrap.
        $scope.showPopupBackend = false;

        $scope.hoverInBackend = function(){
            //console.log('hovering');
            $scope.showPopupBackend = true;

            $timeout(function() {
                $scope.showPopupBackend = false;
                //console.log('hovering timeout');

            }, 5000);
        };

    })



    .controller('CarouselDemoCtrl', function ($scope) {
      $scope.myInterval = 5000;
      $scope.noWrapSlides = false;
      var slides = $scope.slides = [];


    })



;
