angular.module( 'ngBoilerplate.mobile', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'mobile', {
    url: '/mobile',
    views: {
      "main": {
        controller: 'MobileCtrl',
        templateUrl: 'mobile/mobile.tpl.html'
      }
    },
    data:{ pageTitle: 'Mobile' }
  });
})

.controller( 'MobileCtrl', function MobileCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})

    .controller('CarouselDemoCtrl', function ($scope) {
      $scope.myInterval = 3000;
      $scope.noWrapSlides = false;
      var slides = $scope.slides = [];


    })


;
