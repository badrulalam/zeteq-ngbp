angular.module( 'ngBoilerplate.webapp', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'webapp', {
    url: '/webapp',
    views: {
      "main": {
        controller: 'WebappCtrl',
        templateUrl: 'webapp/webapp.tpl.html'
      }
    },
    data:{ pageTitle: 'Web App' }
  });
})

.controller( 'WebappCtrl', function WebappCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})



    .controller('CarouselDemoCtrl', function ($scope) {
      $scope.myInterval = 5000;
      $scope.noWrapSlides = false;
      var slides = $scope.slides = [];


    })



;
